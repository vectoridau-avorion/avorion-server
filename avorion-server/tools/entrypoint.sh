#!/bin/sh

# Use exec to set PID=1 (allows SIGTERM passthrough)
exec python3 ${AVORION_TOOLS_PATH}/wrapper/main.py
