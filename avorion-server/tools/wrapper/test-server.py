#!/usr/bin/env python3

import sys
import time
import random

def w(message, wait=0):
    sys.stderr.write(f'{message}\n')
    sys.stderr.flush()
    time.sleep(wait)

def main():
    w('Starting server...', random.random())
    w('Booting thing 1 of 3...', random.random())
    w('Booting thing 2 of 3...', random.random())
    w('Booting thing 3 of 3...', random.random())
    w('Server is running.')

    for line in sys.stdin:
        command = line.strip()
        w(f'ECHO >> {command}')

        if command == '/save':
            w('Saving server state.', random.random())
            w('Server saved.')


        elif command == '/stop':
            w('Shutting down server...', random.random())
            w('Stopping thing 1 of 3...', random.random())
            w('Stopping thing 2 of 3...', random.random())
            w('Stopping thing 3 of 3...', random.random())
            w('Server is stopped.')
            return


if __name__ == '__main__':
    main()
