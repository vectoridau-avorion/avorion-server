#!/usr/bin/env python3

# Author: Daniel 'Vector' Kerr <vector@vector.id.au>
#
import logging
import os
import signal
import sys
import time
from subprocess import Popen
from subprocess import PIPE
from threading import Thread
from threading import Event


def make_process_args(server_script, galaxy_name, steam_id, data_path):
    return [
        server_script,
        '--galaxy-name', galaxy_name,
        '--admin', steam_id,
        '--datapath', data_path,
    ]


def main():
    # Gather environment variables
    SERVER_PATH = os.getenv('AVORION_SERVER_PATH', '/opt/avorion/server')
    SERVER_BIN = os.getenv('AVORION_SERVER_BIN', 'server.sh')
    DATA_PATH = os.getenv('AVORION_DATA_PATH', '/opt/avorion/data')
    GALAXY_NAME = os.getenv('AVORION_GALAXY_NAME', 'avorion_galaxy')
    STEAM_ID = os.getenv('STEAM_ID', '0')

    # Build the args required to boot the avorion server
    process_args = make_process_args(
        server_script=os.path.join(SERVER_PATH, SERVER_BIN),
        galaxy_name=GALAXY_NAME,
        steam_id=STEAM_ID,
        data_path=DATA_PATH,
    )

    # Uncomment to use the test server
    # process_args = ["python3", "test-server.py"]

    # Create a thread-safe shutdown event
    shutting_down = Event()

    # Instruct the subprocess to ignore the SIGINT and
    # SIGTERM as they are handled by the rest of the
    # application.
    def preexec_ignore_signals():
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        signal.signal(signal.SIGTERM, signal.SIG_IGN)

    # Start the Avorion Server
    logging.info('Starting avorion server wrapper')
    process = Popen(process_args,
                    stdin=PIPE, stdout=sys.stdout, stderr=sys.stderr,
                    preexec_fn=preexec_ignore_signals)

    # Define the shutdown method to execute when SIGINT or
    # SIGTERM are encountered
    def shutdown(signal, frame):
        # If we're already shutting down, ignore further signals
        if shutting_down.is_set():
            return

        # Set the shutting down flag
        shutting_down.set()

        # Send /save followed by /stop
        logging.info(f'Received signal {signal}: shutting down')
        process.stdin.write(b'/save\n')
        process.stdin.flush()
        time.sleep(0.2)
        process.stdin.write(b'/stop\n')
        process.stdin.flush()
        time.sleep(0.2)

    # On SIGINT (CTRL+C) perform safe shutdown
    logging.info('Binding SIGINT to avorion wrapper shutdown method')
    signal.signal(signal.SIGINT, shutdown)

    # On SIGTERM (e.g., docker stop command) perform safe shutdown
    logging.info('Binding SIGTERM to avorion wrapper shutdown method')
    signal.signal(signal.SIGTERM, shutdown)

    # Wait for the process to terminate
    process.wait()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
