# Avorion Server Docker Image

This docker image provides you with a working copy of the Avorion server, ready to run
in a container (or three!)


# IMPORTANT NOTICE

By using this image, you assert that you agree to the
[STEAM LICENSE AGREEMENT](https://store.steampowered.com/subscriber_agreement/).

In order to provide a quick and easy experience for you, this Docker image automatically
agrees to the STEAM LICENSE AGREEMENT when it is built.

This image indicates agreement to the STEAM LICENSE AGREEMENT in order to prepare
the server software for your use, however the acceptance of the content of the
agreement by you is implied by your use of this image.

I have not agreed to this licence on your behalf, and am not connected in any way
to your acceptance of the agreement or use of the software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


## (REALLY) Basic Usage

```sh
docker run -v ./data:/opt/avorion/data vectoridau/avorion-server:latest
```


## Volumes

The following is a list of useful filesystem paths within this Docker image.
Some you probably don't want to touch, and others you definitely want to at least
mount onto your host system (e.g., your data/galaxy path!)

| Variable | Path | Description |
| --- | --- | --- |
| `$AVORION_SERVER_PATH` | `/opt/avorion/server` | Avorion Application Server Files. |
| `$AVORION_DATA_PATH` | `/opt/avorion/data` | Data Path. Mount this volume, or you will lose all progress on shutdown! |
| `$AVORION_TOOLS_PATH` | `/opt/avorion/tools` | Entrypoint and server wrapper script. |
| `$AVORION_MODS_PATH` | `/opt/avorion/mods` | A place for server mods. If you use mods, mounting this volume means your server won't have to download the mods again every time it boots. |


## Wrapper Script / "Safe" Shutdown

This server comes with a small wrapper script which starts the Avorion server as a
subprocess, and captures any SIGINT and SIGKILL signals. When they come through, the
wrapper sends `/save` and `/stop` to the Avorion server.

This does *not* solve the problem of shutting down unsafely (the 10 second docker
shutdown grace period is not likely to be long enough to save your server!) however
it does offer a very small layer of protection, with the possibility of maybe saving
some stuff that you would otherwise lose in an unwelcome or accidental shutdown.


## Server Communication

The recommended way to communicate with this server is in conjunction with an RCON
interface (such as [itzg/rcon](https://hub.docker.com/r/itzg/rcon)).

I originally had the wrapper script include STDIN and a TCP socket for comms, but
it turns out that Avorion has a mechanism built-in, and it would be silly not to
make use of it.

In order to enable RCON, you just need to set an RCON password in your galaxy's
`server.ini` file.


## Docker Composition

Below is a sample docker composition file that you might like to use. It exposes
the necessary ports, mounts some volumes, and sets up an RCON interface.

If you use this file as-is, you'll want to make an empty directory called `data` that
lives next to this file, where all of your galaxy data will go.

To use RCON, all you need to do is enable RCON (edit the galaxy's `server.ini` file),
and add a reference to the avorion server once you log on to the RCON web interface. The
host name of the avorion server is the same as the service name. In the file below,
that would be "avorion-server". (Use this instead of an IP address when configuring the
server entry in RCON).

This stack also spins up a portainer instance that you can use to manage the docker
containers, if you like. If not, just comment out or remove the portainer service.


`docker-compose.yml`
```yml
version: '3'

volumes:
  portainer-data:
  avorion-mods:
  rcon-data:

services:

  # Portainer
  # This is used to manage docker containers. You can log in and start/stop/restart
  # the various containers (or the whole stack, if you like) through a web interface.
  #
  # This server stores data but we don't really care too much, so
  # it can be hidden away and attached to a Docker volume.
  #
  portainer:
    image: portainer/portainer:latest
    volumes:
      - portainer-data:/data:rw
      # Linux
      - /var/run/docker.sock:/var/run/docker.sock
      # Windows
      #- \\.\pipe\docker_engine:\\.\pipe\docker_engine
    ports:
      - 8000:8000/tcp
      - 9000:9000/tcp
    restart: always
    
  # Avorion server
  #
  # This setup expects a folder in the same path as this file called `data`.
  # Your galaxy files (including server.ini) will end up in here.
  #
  # You should set your Steam ID here to default yourself as an admin.
  #
  avorion-server:
    image: vectoridau/avorion-server:latest
    volumes:
      - ./data:/opt/avorion/data:rw
      - avorion-mods:/opt/avorion/mods:rw
    ports:
      - 27000:27000/tcp
      - 27000:27000/udp
      - 27003:27003/udp
      - 27020:27020/udp
      - 27021:27021/udp

    # Change `0` to your own Steam ID so you get admin rights
    environment:
      STEAM_ID: 0
    
    restart: unless-stopped
  
  # RCON server
  # 
  # Sets up an RCON web interface that you can use to communicate
  # with the avorion server without having to muck around with
  # terminal windows or remote desktop connections.
  #
  # This server stores data but we don't really care too much, so
  # it can be hidden away and attached to a Docker volume.
  rcon:
    image: itzg/rcon
    volumes:
      - rcon-data:/opt/rcon-web-admin/db:rw
    ports:
      - 4326:4326/tcp
      - 4327:4327/tcp
    restart: unless-stopped
```

# Authors

Daniel 'Vector' Kerr (<vector@vector.id.au>)
